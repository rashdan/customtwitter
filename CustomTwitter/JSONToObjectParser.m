//
//  JSONToObjectParser.m
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import "JSONToObjectParser.h"
#import "UserFeed.h"

@implementation JSONToObjectParser

+(NSArray*)parseUserFeed:(NSDictionary*)dict{
    if (  [[dict objectForKey:@"feeds"] isEqual:[NSNull null]] )
    {
        NSLog(@"API SENT ERROR OR NULL");
        return nil;
    }
    
    NSString* jsonfield = @"feeds";
    NSArray* dictArr = [[NSArray alloc] initWithArray:[dict objectForKey:jsonfield]];
    NSMutableArray *rtn  = [[NSMutableArray alloc] initWithCapacity:dictArr.count];
    
    for (NSDictionary *d in dictArr)
    {
        if ([d isEqual:[NSNull null]])
        {
            NSLog(@"item object == NULL, %@", NSStringFromSelector(_cmd));
        } else
        {
            UserFeed *aFeedObject = [[UserFeed alloc] init];
            aFeedObject.tweetId = [[d objectForKey:@"id"] integerValue];
            aFeedObject.content = [d objectForKey:@"content"];
            aFeedObject.created_at = [d objectForKey:@"created_at"];
            
            NSDictionary *userDict = [d objectForKey:@"user"];
            if ([userDict isEqual:[NSNull null]])
            {
                NSLog(@"Warning! game object == NULL, %@", NSStringFromSelector(_cmd));
            } else
            {
                aFeedObject.name = [userDict objectForKey:@"name"];
                aFeedObject.userId = [[userDict objectForKey:@"id"] integerValue];
                aFeedObject.email = [userDict objectForKey:@"email"];
                aFeedObject.is_owner = [[userDict objectForKey:@"is_owner"] boolValue];
                
                
            }
            [rtn addObject:aFeedObject];
        }
    }
    
    
    return rtn;
}

@end
