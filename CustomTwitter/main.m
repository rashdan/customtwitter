//
//  main.m
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/30/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
