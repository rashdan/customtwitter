//
//  JSONToObjectParser.h
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONToObjectParser : NSObject

+(NSArray*)parseUserFeed:(NSDictionary*)dict;

@end
