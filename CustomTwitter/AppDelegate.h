//
//  AppDelegate.h
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/30/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

