//
//  CTAPIManager.m
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//



#import "CTAPIManager.h"
#import "JSONToObjectParser.h"
#import "AFHTTPRequestOperationManager.h"
#import "AppConstants.h"


@implementation CTAPIManager

-(void) getUserFeedforToken:(NSString *) token
{
    NSString *url = [NSString stringWithFormat:@"%@%@",CUSTOM_TWITTER_SERVER,SERVER_GET_FEED];
    
    NSDictionary* params;
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:token,@"token",nil];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",token] forHTTPHeaderField:@"Authorization"];
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (_delegate!=nil && ![_delegate isEqual:[NSNull null]] )
        {
            if( [_delegate respondsToSelector:@selector(didReceiveObject:withCode:)])
            {
                [_delegate didReceiveObject:[JSONToObjectParser parseUserFeed:responseObject] withCode:REQUEST_GET_FEED];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if ([_delegate respondsToSelector:@selector(request:didFailWithError:)])
        {
            [_delegate request:REQUEST_GET_FEED didFailWithError:error];
        }
    }];
    
    
}

-(void)getFeedsWithCompletionHandler:(void (^)(BOOL, NSArray *, NSError *))completionHandler{
    
    NSString *url = [NSString stringWithFormat:@"%@%@",CUSTOM_TWITTER_SERVER,SERVER_GET_FEED];
    
    NSDictionary* params;
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:USER_API_TOKEN,@"token",nil];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Token token=%@",USER_API_TOKEN] forHTTPHeaderField:@"Authorization"];
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        completionHandler(true, [JSONToObjectParser parseUserFeed:responseObject], nil);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        completionHandler(true, nil, error);
    }];
}


@end
