//
//  FeedCollectionViewCell.h
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel* usernameLabel;

@property (nonatomic, weak) IBOutlet UILabel* tweetLabel;

@property (nonatomic, weak) IBOutlet UILabel* dateLabel;

@property (nonatomic, weak) IBOutlet UIImageView* profileImageView;

@property (nonatomic, weak) IBOutlet UIView* profileImageContainer;

@property (nonatomic, weak) IBOutlet UIImageView* bgImageView;

@end
