//
//  UserFeed.h
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserFeed : NSObject


@property (nonatomic) NSInteger tweetId;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *created_at;
@property (nonatomic) NSInteger userId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;
@property (nonatomic) BOOL is_owner;


@end
