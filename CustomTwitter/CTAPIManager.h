//
//  CTAPIManager.h
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"

#define CUSTOM_TWITTER_SERVER @"https://try-ror.herokuapp.com/"
#define SERVER_GET_FEED @"/api/feeds"
#define SERVER_SIGN_UP_USER @"/api/users"


#define REQUEST_GET_FEED 100
#define REQUEST_PUT_SIGNUP_USER 101

@protocol CTAPIManagerDelegate<NSObject>

-(void)didReceiveObject:(id)object withCode:(int)reqCode;
-(void)request:(int)reqCode didFailWithError:(NSError *)error;

@end

@interface CTAPIManager : NSObject

@property (nonatomic,weak) id<CTAPIManagerDelegate> delegate;

-(void) getUserFeedforToken:(NSString *) token;
//-(void) signUpUserWithUserName:(NSString *)username email:(NSString *)email andPassword:(NSString *)password;
-(void)getFeedsWithCompletionHandler:(void (^)(BOOL, NSArray *, NSError *))completionHandler;
@end
