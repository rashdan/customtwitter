//
//  ViewController.h
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/30/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CHTCollectionViewWaterfallLayout.h>
#import "CTAPIManager.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, CHTCollectionViewDelegateWaterfallLayout, CTAPIManagerDelegate>
{
    CTAPIManager *_apiManager;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
-(void)fetchNewDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
@end

