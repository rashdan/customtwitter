//
//  FeedCollectionViewCell.m
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/31/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import "FeedCollectionViewCell.h"
#import "AppConstants.h"

@implementation FeedCollectionViewCell

-(void)awakeFromNib {
    [self.bgImageView setImage:[[UIImage imageNamed:@"feedBG.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    
    [self.usernameLabel setFont:[UIFont fontWithName:HEADING_FONT_NAME size:USER_NAME_LABEL_SIZE]];
    
    [self.tweetLabel setFont:[UIFont fontWithName:TWEET_FONT_NAME size:TWEET_LABEL_SIZE]];
    
    [self.dateLabel setFont:[UIFont fontWithName:HEADING_FONT_NAME size:TWEET_DATE_SIZE]];
    
    [self.usernameLabel setTextColor:USER_NAME_LABEL_COLOR];
    [self.tweetLabel setTextColor:TWEET_LABEL_COLOR];
    [self.dateLabel  setTextColor:TWEET_DATE_COLOR];
    
    [self.usernameLabel setShadowColor:[UIColor whiteColor]];
    [self.usernameLabel setShadowOffset:CGSizeMake(0, 1)];
    
    [self.tweetLabel setShadowColor:[UIColor whiteColor]];
    [self.tweetLabel setShadowOffset:CGSizeMake(0, 1)];
    
    [self.dateLabel setShadowColor:[UIColor whiteColor]];
    [self.dateLabel setShadowOffset:CGSizeMake(0, 1)];
    
}

@end
