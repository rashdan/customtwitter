//
//  ViewController.m
//  CustomTwitter
//
//  Created by Rashdan Natiq on 10/30/15.
//  Copyright © 2015 Rashdan. All rights reserved.
//

#import "ViewController.h"
#import "CTAPIManager.h"
#import "AppConstants.h"
#import "FeedCollectionViewCell.h"
#import <NSDate+MinimalTimeAgo.h>
#import "UserFeed.h"

@interface ViewController ()

@property (strong, atomic) NSArray *tweets;

@property (strong, atomic) NSMutableDictionary *imagesDictionary;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:22];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    titleLabel.text = @"Custom Twitter";
    self.navigationItem.titleView = titleLabel;
    
    self.imagesDictionary = [NSMutableDictionary dictionary];
    
    
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    layout.headerHeight = 15;
    layout.footerHeight = 10;
    layout.minimumColumnSpacing = 20;
    layout.minimumInteritemSpacing = 30;
    
    self.collectionView.collectionViewLayout = layout;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupCollectionView];
    [self refreshTwitterFeed];
}

- (void)refreshTwitterFeed {
    
    if (_apiManager == nil)
    {
        _apiManager = [[CTAPIManager alloc] init];
        
    }
    _apiManager.delegate = self;
    
    [_apiManager getUserFeedforToken:USER_API_TOKEN];
}



#pragma mark - UICollectionView datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tweets.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FeedCollectionViewCell *cell = (FeedCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"TweetCell" forIndexPath:indexPath];
    
    UserFeed *tweet = self.tweets[indexPath.row];
    
    
    cell.usernameLabel.text = tweet.name;
    cell.tweetLabel.text = tweet.content;
    
    cell.tweetLabel.frame =
    CGRectMake(cell.tweetLabel.frame.origin.x,
               cell.tweetLabel.frame.origin.y,
               cell.bounds.size.width - 84,
               [self heightForCellAtIndex:indexPath.row]-50);
    cell.tweetLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    cell.tweetLabel.font = [UIFont systemFontOfSize:13.0f];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    NSDate *date = [dateFormatter dateFromString:tweet.created_at];
    NSString *dateString = [NSString stringWithFormat:@"%@ ago", [date timeAgo]];
    cell.dateLabel.text = dateString;
    
    return cell;
}


#pragma mark - UICollectionView delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat width = [(CHTCollectionViewWaterfallLayout *)collectionViewLayout itemWidthInSectionAtIndex:indexPath.section];
    
    return CGSizeMake(width, [self heightForCellAtIndex:indexPath.row]);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(CHTCollectionViewWaterfallLayout *)collectionViewLayout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightForCellAtIndex:indexPath.row];
}



- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
        return UIEdgeInsetsMake(20, 10, 20, 10);
    }
    else{
        return UIEdgeInsetsMake(20, 12, 20, 12);
    }
    
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self setupCollectionView];
}

- (void)setupCollectionView {
    
    NSInteger colCount = 2;
    UIEdgeInsets sectionInset = UIEdgeInsetsMake(20, 12, 20, 12);
    if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
        
        colCount = 3;
        sectionInset = UIEdgeInsetsMake(20, 10, 20, 10);
    }
    CHTCollectionViewWaterfallLayout *layout = (CHTCollectionViewWaterfallLayout *)self.collectionView.collectionViewLayout;
    //layout.itemWidth = width;
    layout.columnCount = colCount;
    layout.sectionInset = sectionInset;
    [self.collectionView reloadData];
}

- (CGFloat)heightForCellAtIndex:(NSUInteger)index {
    
    UserFeed *tweet = self.tweets[index];
    CGFloat cellHeight = 55;
    NSString *tweetText = tweet.content;
    
    CHTCollectionViewWaterfallLayout *layout = (CHTCollectionViewWaterfallLayout *)self.collectionView.collectionViewLayout;
    CGFloat width = [layout itemWidthInSectionAtIndex:0];
    CGSize labelHeight = [tweetText sizeWithFont:[UIFont systemFontOfSize:13.0f] constrainedToSize:CGSizeMake(width - 84, 4000)];
    
    cellHeight += labelHeight.height;
    return cellHeight;
}

-(void)getImageFromUrl:(NSString*)imageUrl asynchronouslyForImageView:(UIImageView*)imageView andKey:(NSString*)key{
    
    dispatch_async(dispatch_get_global_queue(
                                             DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *url = [NSURL URLWithString:imageUrl];
        
        __block NSData *imageData;
        
        dispatch_sync(dispatch_get_global_queue(
                                                DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            imageData =[NSData dataWithContentsOfURL:url];
            
            if(imageData){
                
                [self.imagesDictionary setObject:[UIImage imageWithData:imageData] forKey:key];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    imageView.image = self.imagesDictionary[key];
                });
            }
        });
    });
}


#pragma mark - CTAPIManagerDelegate

-(void)didReceiveObject:(id)object withCode:(int)reqCode
{
    switch (reqCode) {
        case REQUEST_GET_FEED:
        {
            if (object != nil) {
                self.tweets = object;
            }
            
            [self.collectionView reloadData];
            break;
        }
            
        default:
            break;
    }
    
    
}


-(void) request:(int)reqCode didFailWithError:(NSError *)error
{
    switch (reqCode) {
            
        case REQUEST_GET_FEED:
        {
            [self showRetryAlertWithError:error];
        }
            break;
            
            
        default:
            break;
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex == alertView.firstOtherButtonIndex) {
        [self refreshTwitterFeed];
    }
}

#pragma mark - Private methods

- (void)showRetryAlertWithError:(NSError *)error {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error fetching data", @"")
                                                        message:error.localizedDescription
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Dismiss", @"")
                                              otherButtonTitles:NSLocalizedString(@"Retry", @""), nil];
    [alertView show];
}

-(void)fetchNewDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    [_apiManager getFeedsWithCompletionHandler:^(BOOL success, NSArray *dataArray, NSError *error) {
        if (success) {
            
            NSInteger oldTweetCount = [self.tweets count];
            NSInteger currentTweetCount = [dataArray count];
            
            
            if (oldTweetCount == currentTweetCount) {
                completionHandler(UIBackgroundFetchResultNoData);
                
                // no new data
            }
            else{
                self.tweets = dataArray;
                [self.collectionView reloadData];
                completionHandler(UIBackgroundFetchResultNewData);
                
                // new data found
            }
        }
        else{
            completionHandler(UIBackgroundFetchResultFailed);
            
            // failed to fetch new tweets
        }
    }];
}

@end
